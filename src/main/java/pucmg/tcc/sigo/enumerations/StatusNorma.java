package pucmg.tcc.sigo.enumerations;

public enum StatusNorma {
    A("A"), /*ATIVA,*/
    R("R")  /*REVOGADA*/;

    private final String valorString;

    StatusNorma(String e) {
        valorString = e;
    }
}
