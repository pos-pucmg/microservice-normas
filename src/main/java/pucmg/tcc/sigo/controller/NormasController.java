package pucmg.tcc.sigo.controller;

import java.io.*;
import java.sql.SQLException;
import java.util.List;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import pucmg.tcc.sigo.entities.NormaAplicavel;
import pucmg.tcc.sigo.enumerations.StatusNorma;
import pucmg.tcc.sigo.enumerations.TipoNorma;
import pucmg.tcc.sigo.service.NormasService;

import javax.servlet.http.HttpServletResponse;

@CrossOrigin
@RestController
@RequestMapping(value="/sigo/mgn")
public class NormasController {

    @Autowired
    NormasService normasService;

    @PostMapping(value = "/importar")
    public ResponseEntity<Void> importar(@RequestBody NormaAplicavel normaShortInfo) {
        NormaAplicavel norma = normasService.salvarNormaRepositorioLocal(normaShortInfo,
                TipoNorma.E,
                normasService.getNormaRepositorioExterno(normaShortInfo.getId())
        );
        if(norma == null) return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value="/normas", produces = "application/json; charset=UTF-8")
    @ResponseBody
    public ResponseEntity<List<NormaAplicavel>> buscarRepositorioLocal(@RequestParam(value = "codigo",required = false) String codigo) {
        List<NormaAplicavel> listaInfoNormas = normasService.getResumoNormasRepositorioLocal(codigo);
        if(CollectionUtils.isEmpty(listaInfoNormas)) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(listaInfoNormas, HttpStatus.OK);
    }

    @GetMapping(value="/externo/normas", produces = "application/json; charset=UTF-8")
    @ResponseBody
    public ResponseEntity<List<NormaAplicavel>> buscarRepositorioExterno(@RequestParam(value = "codigo",required = false) String codigo) {
        List<NormaAplicavel> listaInfoNormas = normasService.getResumoNormasRepositorioExterno(codigo);
        if(CollectionUtils.isEmpty(listaInfoNormas)) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(listaInfoNormas, HttpStatus.OK);
    }

    @Transactional
    @GetMapping(value="/norma/{id}",produces = "application/pdf")
    public void visualizarNorma(@PathVariable("id") String id,
                            HttpServletResponse response) throws SQLException, IOException {
        NormaAplicavel norma = normasService.getNormaRepositorioLocal(id);
        if(norma == null) {
            response.sendError(HttpStatus.NOT_FOUND.ordinal());
            return;
        }
        response.addHeader("Content-Type", String.valueOf(MediaType.parseMediaType("application/pdf")));
        response.addHeader("Content-Disposition", "filename=" + norma.getCodigo());
        IOUtils.copy(norma.getNormaFile().getBinaryStream(), response.getOutputStream());
    }

    @Transactional
    @GetMapping(value="/download/norma/{id}",produces = "application/pdf")
    public void baixarNorma(@PathVariable("id") String id,
                            HttpServletResponse response) throws SQLException, IOException {
        NormaAplicavel norma = normasService.getNormaRepositorioLocal(id);
        if(norma == null) {
            response.sendError(HttpStatus.NOT_FOUND.ordinal());
            return;
        }
        response.addHeader("Content-Type", String.valueOf(MediaType.parseMediaType("application/pdf")));
        response.addHeader("Content-Disposition", "attachment; filename=" + norma.getCodigo());
        IOUtils.copy(norma.getNormaFile().getBinaryStream(), response.getOutputStream());
    }
}
