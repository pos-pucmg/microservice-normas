package pucmg.tcc.sigo.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;
import pucmg.tcc.sigo.entities.NormaAplicavel;
import pucmg.tcc.sigo.enumerations.StatusNorma;

import java.util.List;

@Repository
public interface NormaAplicavelRepository extends CrudRepository<NormaAplicavel,Long> {
    List<NormaAplicavel> findAll();

    @Query(value = "SELECT new NormaAplicavel (n.id, n.codigo, n.ano , n.titulo, n.descricao, " +
                   "            n.dataImportacao, n.statusNorma, n.tipoNorma, n.dataAtivacao, n.dataInativacao) " +
                   "FROM NormaAplicavel n " +
                   "WHERE n.codigo LIKE :codigo")
    List<NormaAplicavel> findShortInfo(String codigo);

    @Query(value =  "SELECT new NormaAplicavel (n.id, n.codigo, n.ano , n.titulo, n.descricao, " +
                    "            n.dataImportacao, n.statusNorma, n.tipoNorma, n.dataAtivacao, n.dataInativacao) " +
                    "FROM NormaAplicavel n ")
    List<NormaAplicavel> findShortInfo();

    NormaAplicavel findById(String id);

    List<NormaAplicavel> findAllByStatusNormaIn(@Nullable List<StatusNorma> statusNormaList);
    List<NormaAplicavel> findAllByCodigoContains(String codigoNorma);

}
