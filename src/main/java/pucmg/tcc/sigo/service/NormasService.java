package pucmg.tcc.sigo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import pucmg.tcc.sigo.entities.NormaAplicavel;
import pucmg.tcc.sigo.enumerations.TipoNorma;
import pucmg.tcc.sigo.repositories.NormaAplicavelRepository;

import javax.sql.rowset.serial.SerialBlob;
import java.io.*;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;

@Service
public class NormasService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private NormaAplicavelRepository normaAplicavelRepository;

    private static final String REPOSITORIO_EXTERNO_SERVICE_BASEURL = "SIGO-ALB-129018962.sa-east-1.elb.amazonaws.com/repositorio";

    public List<NormaAplicavel> getResumoNormasRepositorioLocal(String codigo) {
        if(StringUtils.isEmpty(codigo))
            return normaAplicavelRepository.findShortInfo();

        return normaAplicavelRepository.findShortInfo('%'+codigo+'%');
    }

    public List<NormaAplicavel> getResumoNormasRepositorioExterno(String searchPattern) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(getServiceURL("/normas"))
                .queryParam("codigo", searchPattern);
        return (List<NormaAplicavel>) restTemplate.getForObject(builder.toUriString(), List.class);

    }

    public NormaAplicavel getNormaRepositorioLocal(String idNorma) {
        return normaAplicavelRepository.findById(idNorma);
    }

    public NormaAplicavel salvarNormaRepositorioLocal(NormaAplicavel normaShortInfo, TipoNorma tipoNorma, Blob norma) {
        NormaAplicavel normaAplicavel = new NormaAplicavel(normaShortInfo,tipoNorma,norma);
        return normaAplicavelRepository.save(normaAplicavel);
    }

    public SerialBlob getNormaRepositorioExterno(String idNorma){
        return restTemplate.execute(
                getServiceURL("/download/norma/{idNorma}"),
                HttpMethod.GET,
                null,
                response -> {
                    InputStream normaInputStream = response.getBody();
                    HttpHeaders headers = response.getHeaders();
                    int contentLength = (int) headers.getContentLength();
                    byte[] normaByteArray = new byte[contentLength];
                    int totalRead = 0;
                    int read;
                    while(totalRead < contentLength) {
                        read = normaInputStream.read(normaByteArray,totalRead,contentLength-totalRead);
                        if(read > 0) totalRead += read;
                    }
                    try {
                        return new SerialBlob(normaByteArray);
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    } finally {
                        normaInputStream.close();
                    }

                    return null;
                }, idNorma);
    }

    private String getServiceURL(String serviceEndpoint){
        return "http://".concat(REPOSITORIO_EXTERNO_SERVICE_BASEURL).concat(serviceEndpoint);
    }
}
