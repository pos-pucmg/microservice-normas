package pucmg.tcc.sigo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ModuloNormasApplication {

    public static void main(String[] args) {
        SpringApplication.run(ModuloNormasApplication.class, args);
    }

}
