package pucmg.tcc.sigo.entities;


import java.io.Serializable;

public class NormaAplicavelId implements Serializable {
    private String codigoNorma;
    private String versaoNorma;

    public String getCodigoNorma() {
        return codigoNorma;
    }

    public void setCodigoNorma(String codigoNorma) {
        this.codigoNorma = codigoNorma;
    }

    public String getVersaoNorma() {
        return versaoNorma;
    }

    public void setVersaoNorma(String versaoNorma) {
        this.versaoNorma = versaoNorma;
    }
}
