package pucmg.tcc.sigo.entities;

import pucmg.tcc.sigo.enumerations.StatusNorma;
import pucmg.tcc.sigo.enumerations.TipoNorma;

import javax.persistence.*;
import java.sql.Blob;
import java.util.Date;

@Entity
//@IdClass(NormaAplicavelId.class)
@Table(name="T_NORMA")
public class NormaAplicavel {
    @Id
    @Column(name = "ID_NORMA")
    private String id;

    //@Id
    @Column(name = "CD_NORMA")
    private String codigo;

//    @Id
//    @Column(name="VR_NORMA")
//    private String versaoNorma;

    @Column(name = "DT_NORMA")
    private Integer ano;

    @Column(name = "NM_NORMA")
    private String titulo;

    @Column(name = "DS_NORMA")
    private String descricao;

    @Column(name = "TP_NORMA")
    private String tipoNorma;
    @Column(name = "ST_NORMA")
    private String statusNorma;
    /*
        @Enumerated
        @Column(name="TP_NORMA")
        private TipoNorma tipoNorma;

        @Enumerated
        @Column(name="ST_NORMA")
        private StatusNorma statusNorma;
    */
    @Column(name = "DT_INSERCAO")
    private Date dataImportacao;

    @Column(name = "DT_ATIVACAO")
    private Date dataAtivacao;

    @Column(name = "DT_INATIVACAO")
    private Date dataInativacao;

    @Lob
    @Column(name = "BL_NORMA")
    private Blob normaFile;

    public NormaAplicavel() {
    }

    public NormaAplicavel(NormaAplicavel clone, TipoNorma tipo, Blob normaFile) {
        this.id = clone.getId();
        this.codigo = clone.getCodigo();
        this.ano = clone.getAno();
        this.titulo = clone.getTitulo();
        this.descricao = clone.getDescricao();
        this.statusNorma = StatusNorma.A.name();
        this.tipoNorma = tipo.name();
        this.dataImportacao = new Date();
        this.dataInativacao = null;
        this.normaFile = normaFile;
    }

    public NormaAplicavel(String id, String codigo, Integer ano, String titulo, String descricao,
                          Date dataImportacao, String status, String tipo,
                          Date dataAtivacao, Date dataInativacao) {
        this.id = id;
        this.codigo = codigo;
        this.ano = ano;
        this.titulo = titulo;
        this.descricao = descricao;
        this.statusNorma = status;
        this.tipoNorma = tipo;
        this.dataAtivacao = dataAtivacao;
        this.dataImportacao = dataImportacao;
        this.dataInativacao = dataInativacao;
    }

    public String getId() {
        return id;
    }

    public String getCodigo() {
        return codigo;
    }

    public Integer getAno() {
        return ano;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public String getTipoNorma() {
        return tipoNorma;
    }

    public String getStatusNorma() {
        return statusNorma;
    }

    public Date getDataImportacao() {
        return dataImportacao;
    }

    public Date getDataAtivacao() {
        return dataAtivacao;
    }

    public Date getDataInativacao() {
        return dataInativacao;
    }

    public Blob getNormaFile() {
        return normaFile;
    }
}
