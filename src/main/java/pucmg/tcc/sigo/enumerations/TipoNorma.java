package pucmg.tcc.sigo.enumerations;

public enum TipoNorma {
    I("I"), /*INTERNA,*/
    E("E")  /*EXTERNA*/;

    private final String valorString;

    TipoNorma(String e) {
        valorString = e;
    }
}
